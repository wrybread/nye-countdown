## New Year's Eve Clock!

It's always turning midnight somewhere in the world, and this clock tells you where. So you can celebrate on New Year's Eve at the turn of every hour, not just midnight.

Designed to be displayed on a projector or large TV at a New Year's Eve party.

![screenshot1](screenshots/screenshot1.jpg)

Shows a video countdown (countdown.mp4) during the last 10 seconds before the hour change using the Python VLC bindings (included here, or delete to use your own). You'll need VLC installed to use it.

Uses Pygame, with the ptext extension (also included here) to do the outline around the text. Tested on Win7 with Python 2.7.

---

## Additional Features

On Windows can press the 1 key to move the window to monitor 1, 2 key to move to monitor 2, 3 key to move off screen.

Note that the location and size of these is hard coded and should be customized for your setup.

There's a few variables you can set at the top of the script.


---

## Cities

Here are the cities used, can obviously be changed. This should really be changed to GMT relative to make it more portable:

time_zones[0] = "Anchorage"

time_zones[1] = "Honolulu"

time_zones[2] = "Samoa"

time_zones[3] = "Fiji"

time_zones[4] = "Solomon Islands" 

time_zones[5] = "Guam" 

time_zones[6] = "Osaka"

time_zones[7] = "Bali"

time_zones[8] = "Bangkok" 

time_zones[9] = "Bangladesh" # GMT+6 

time_zones[10] = "Karachi"

time_zones[11] = "Dubai" 

time_zones[12] = "Madigascar" 

time_zones[13] = "Cairo"

time_zones[14] = "Paris"

time_zones[15] = "London"

time_zones[16] = "The Azores" # or Cape Verde 

time_zones[17] = "The Sandwich Islands"  # GMT-2, not many options!

time_zones[18] = "Buenos Aires"

time_zones[19] = "Puerto Rico" 

time_zones[20] = "Havana and NYC" 

time_zones[21] = "New Orleans" # (also Acapulco)

time_zones[22] = "Denver" 

time_zones[23] = "San Francisco" 

time_zones[24] = "Anchorage" 



---

## To do

Order the cities by GMT time and select the appropriate city using a timedelta to make it more portable. Currently it's hard coded for San Francisco (GMT-8) since I made it in a rush without thinking about open sourcing it. If you fix that please let me know!

Should be easy enough to reorder the cities for a quick and dirty fix though.
