#! /usr/bin/python



from datetime import datetime
import time, os, sys
from Queue import Queue

import pygame
from pygame.locals import *

import vlc # included in the directory, but from https://wiki.videolan.org/python_bindings

import ptext # for the text stroke, from https://github.com/cosmologicon/pygame-text


'''

Features:

- shows where it's turning midnight at any hour, especially for us folks near the dateline

- in last minute shows seconds, in last 10 seconds shows a countdown. Then shows "they're kissing in _____".

- press 1 key to go to mon 1, 2 key to drag to monitor 2, 3 key to drag off screen

- can hide frame and launch on mon2. No need for fullscreen, just make size of projector

- enter key toggles fullscreen, esc exits




To do:

- other countdown videos?

- test with secondary monitor....

- add places with half hour offset times, like calcutta (gmt+5.5)? 

- rework timezones using GMT time and time deltas...

- currently monitor 2 window position is hard coded....

'''



if getattr(sys, 'frozen', False):
    current_directory = os.path.dirname(sys.executable)
elif __file__:
    # unfrozen, running from script
    current_directory = os.path.dirname(__file__)




# size of the window
screen_size = (1024, 768)

# the font for each line
font1_fname = os.path.abspath(current_directory + "/fonts/FontdSpaTT.ttf")
font2_fname = os.path.abspath(current_directory + "/fonts/FontdSpaTT.ttf")
font3_fname = os.path.abspath(current_directory + "/fonts/FontdSpaTT.ttf")

# font size for each line
font1_size = 100
font2_size = 100
font3_size = 88

# font color for each line
font_color1 = (255, 255, 255)
font_color2 = font_color1
font_color3 = font_color1


# use this to move all lines up
v_offset = -100


# set the window position at startup? (Windows)
move_window = False
xpos = 400
ypos = 0


# launch the window with a frame?
show_window_frame = True







# not sure if necessary
# Enable in Windows to use directx renderer instead of windib
#os.environ["SDL_VIDEODRIVER"] = "directx"

pygame.init()


if show_window_frame: screen = pygame.display.set_mode(screen_size)
else: screen = pygame.display.set_mode(screen_size, pygame.NOFRAME)
    
#screen = pygame.display.set_mode(screen_size, pygame.FULLSCREEN)
#screen = pygame.display.set_mode(screen_size, pygame.RESIZABLE)


# set the program icon
icon = pygame.image.load(current_directory + "/icon.png")
pygame.display.set_icon(icon)

pygame.display.set_caption('NYE Countdown')
pygame.display.get_wm_info() # necessary?
clock = pygame.time.Clock()


#print "Using %s renderer" % pygame.display.get_driver()



# load the fonts
font1 = pygame.font.Font(font1_fname, font1_size)
font2 = pygame.font.Font(font2_fname, font2_size)
font3 = pygame.font.Font(font3_fname, font3_size)

# load the background graphics
fireworks1 = os.path.abspath(current_directory + "/fireworks1.png") # shown during regular messages
fireworks2 = os.path.abspath(current_directory + "/fireworks2.png") # shown during "they're kissing in ___"
bg1 = pygame.image.load(fireworks1)
bg2 = pygame.image.load(fireworks2)
bg_graphic = bg1 



# List of time zones
# Should be listed in relation to GMT...
# Usepage: https://www.timeanddate.com/worldclock/full.html?sort=2

time_zones = {}
time_zones[0] = "Anchorage"
time_zones[1] = "Honolulu"
time_zones[2] = "Samoa"
time_zones[3] = "Fiji" # or Iceland?

time_zones[4] = "Solomon Islands" 
time_zones[5] = "Guam" 
time_zones[6] = "Osaka"
time_zones[7] = "Bali"

time_zones[8] = "Bangkok" 
time_zones[9] = "Bangladesh" # GMT+6 
time_zones[10] = "Karachi"
time_zones[11] = "Dubai" 
time_zones[12] = "Madigascar" 
time_zones[13] = "Cairo"
time_zones[14] = "Paris"
time_zones[15] = "London"
time_zones[16] = "The Azores" # or Cape Verde 
time_zones[17] = "The Sandwich Islands"  # GMT-2, not many options!
time_zones[18] = "Buenos Aires"
time_zones[19] = "Puerto Rico" 
time_zones[20] = "Havana and NYC" 
time_zones[21] = "New Orleans" # (also Acapulco)
time_zones[22] = "Denver" 
time_zones[23] = "Dillon Beach" 
time_zones[24] = "Anchorage" 

# unused: kingston. Calcutta is GMT+5.5....





def move_window_func(monitor=None, pos=None):

    if not sys.platform.startswith("win"):
         print "window positioning only works on Windows...."
         return
    
    # I coulnd't position the window using SDL_VIDEO_WINDOW_POS so using win32gui
    try: win32gui
    except: import win32gui
    handle = win32gui.GetForegroundWindow()
    rect = win32gui.GetWindowRect(handle)
    width = abs(rect[2]) - abs(rect[0])
    #width = abs(width)
    height = rect[3] - rect[1]

    if pos:
        x = pos[0]
        y = pos[1]

    if monitor == 1:
        x = 0
        y = 0
    elif monitor == 2:
        # to do: detect monitor 2's position...
        x = 1441 # X position of monitor 2
        y = 0
    elif monitor == 3:
        x = 3000 # off screen
        y = 0

    
    win32gui.MoveWindow(handle, x, y, width, height, True)



# position the program window? (only works under Windows)
if move_window:
    move_window_func(pos=[xpos, ypos])


# various variables
running = True
movie_playing = False
last_text = None
start_time = time.time()
movie_queue = Queue()
iterations = 0

while running:

    hourMil = int( datetime.now().strftime('%H') )
    minute_int = int( datetime.now().strftime('%M') )
    secs_int = int( datetime.now().strftime('%S') )
    
    hour = str( int(datetime.now().strftime('%I') ))
    mins = datetime.now().strftime('%M')
    secs = datetime.now().strftime('%S')
    
    microseconds = datetime.now().strftime('.%f')#[:-1]
    microseconds = str(microseconds)[:3]

    screen.fill((0, 0, 0))

    location = time_zones[hourMil]
    
    try: last_location = time_zones[hourMil-1]
    except: last_location = time_zones[24] # not sure about this....

    #print movie_playing, minute_int, secs_int

    # for a few minutes after the hour, say happy new year
    if minute_int <= 2:
        text1 = "12:%sam" % mins
        text2 = "HAPPY NEW YEAR!"
        text3 = "They're kissing in %s!" % last_location
        bg_graphic = bg2

    # normal display (no seconds)
    elif minute_int <= 58:
        text1 = "11:%spm" % mins
        text2 = "in %s" % (location)
        text3 = ""
        bg_graphic = bg1

    # last 10 seconds play the countdown movie
    elif movie_playing == False and minute_int == 59 and secs_int == 50:
        print "Queing the countdown movie!"
        movie_queue.put("countdown.mp4")
        text1 = "   "
        text2 = ""
        text3 = ""
        #movie_playing = True

    # last minute show seconds too
    else:
        text1 = "11:%s:%s" % (mins, secs)
        text2 = "in %s" % (location)
        text3 = ""
        bg_graphic = bg1

    
    iterations += 1

    # only render if the text has changed, or if the iterations are more than 10 (so we render if program window state changes)
    if text1 != last_text or iterations > 10: 

        last_text = text1
        iterations = 0 

        # blit the bg graphic
        screen.blit(bg_graphic, [0,0])

        # blit the lines of text
        SCREEN_WIDTH, SCREEN_HEIGHT = pygame.display.get_surface().get_size()
        text_obj1 = font1.render(text1, True, font_color1)
        text_rect1 = text_obj1.get_rect(center=(SCREEN_WIDTH/2, SCREEN_HEIGHT/2 + v_offset))
        #screen.blit(text_obj1, text_rect1)
        ptext.draw(text1, (text_rect1[0], text_rect1[1]), fontname=font1_fname, fontsize=font1_size, owidth=1.5, ocolor="#000000")
        
        text_obj2 = font2.render(text2, True, font_color2)
        text_rect2 = text_obj2.get_rect(center=(SCREEN_WIDTH/2, SCREEN_HEIGHT/2 + 140 + v_offset))
        #screen.blit(text_obj2, text_rect2)
        ptext.draw(text2, (text_rect2[0], text_rect2[1]), fontname=font2_fname, fontsize=font2_size, owidth=1.5, ocolor="#000000")
        
        text_obj3 = font3.render(text3, True, font_color2)
        text_rect3 = text_obj3.get_rect(center=(SCREEN_WIDTH/2, SCREEN_HEIGHT/2 + 280 + v_offset))
        #screen.blit(text_obj2, text_rect2)
        ptext.draw(text3, (text_rect3[0], text_rect3[1]), fontname=font3_fname, fontsize=font3_size, owidth=1.5, ocolor="#000000") 

        pygame.display.flip()


    if movie_playing:

        try:
            # the movie wouldn't render if I placed this loop after playing the movie, so I moved it to here... Hmm.
            while player.get_state() != vlc.State.Ended and False:
                print time.time(), player.get_state()
                screen.fill((0, 0, 0))
                screen.blit(text_obj1, text_rect1)
                pygame.display.flip()
                clock.tick(60)

            if movie_playing and player.get_state() == vlc.State.Ended:
                print "Stopping (and hiding) the movie player"
                player.stop()
                movie_playing = False
                last_text=None # so it redraws

        except Exception, e:
            print "Error getting playing state: %s" % e
            

    
    #clock.tick(60)


    # only check the movie queue if nothing is playing...
    if not movie_playing:
        while not movie_queue.empty():
            movie_fname = movie_queue.get()
            print "Playing %s!" % movie_fname
            movie_playing = True

            # Create instane of VLC and create reference to movie.
            vlcInstance = vlc.Instance()
            #movie_fname = "countdown1.mp4"
            media = vlcInstance.media_new(movie_fname)        

            # Create new instance of vlc player
            player = vlcInstance.media_player_new()

            # fix it so we receive mouse input
            player.video_set_mouse_input(False)
            player.video_set_key_input(False)
            
            # Pass pygame window id to vlc player, so it can render its contents there.
            player.set_hwnd(pygame.display.get_wm_info()['window'])
            # Load movie into vlc player instance
            player.set_media(media)

            # mute the movie player
            player.audio_set_volume(0)


            # Quit pygame mixer to allow vlc full access to audio device (REINIT AFTER MOVIE PLAYBACK IS FINISHED!)
            #pygame.mixer.quit()

            
            # Start movie playback
            player.play()





    # handle keyboard events
    for event in pygame.event.get():

        #print event.type
        
        if event.type == pygame.QUIT:
            running=False

        elif event.type == KEYDOWN:

            
            if event.key == K_ESCAPE:
                if screen.get_flags() & FULLSCREEN:
                    # exit fullscreen
                    pygame.display.set_mode(screen_size)
                else:
                    # just exit
                    running=False

            elif event.key == 13:
                # toggle fullscreen
                if screen.get_flags() & FULLSCREEN:
                    pygame.display.set_mode(screen_size)
                else:
                    pygame.display.set_mode(screen_size, FULLSCREEN)


            elif event.key == 49: # 1, move to mon1
                move_window_func(1)
            elif event.key == 50: # 2, move to mon2
                move_window_func(2)
            elif event.key == 51: # 3, move off screen
                move_window_func(3)
                


    clock.tick(60)


pygame.quit()
